import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './components/list/list.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FormComponent } from './components/form/form.component';
import { NewFormComponent } from './components/new-form/new-form.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { DeleteBoxComponent } from './components/delete-box/delete-box.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { AppPipesModule } from './app-pipes.module';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ProfileComponent,
    FormComponent,
    NewFormComponent,
    EditFormComponent,
    DeleteBoxComponent,
    HomepageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppPipesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
