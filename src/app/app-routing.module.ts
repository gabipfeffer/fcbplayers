import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './components/list/list.component';
import { ProfileComponent } from './components/profile/profile.component';
import { NewFormComponent } from './components/new-form/new-form.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { DeleteBoxComponent } from './components/delete-box/delete-box.component';
import { HomepageComponent } from './components/homepage/homepage.component';


const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'list', component: ListComponent },
  { path: 'profile/:id', component: ProfileComponent },
  { path: 'form', component: NewFormComponent },
  { path: 'edit/:id', component: EditFormComponent },
  { path: 'delete/:id', component: DeleteBoxComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
