import { Component, OnInit } from '@angular/core';
import { IPlayer } from '../../interfaces/players.interface';

import { PlayersService } from '../../services/players.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.scss']
})
export class NewFormComponent implements OnInit {

  constructor(private playerService: PlayersService,
              private router: Router) { }

  ngOnInit() {
  }

  onFormSubmit(newPlayer: IPlayer) {
    this.playerService.addPlayer(newPlayer).toPromise().then(() => {
      this.router.navigate(['/list']);
    });
  }
}
