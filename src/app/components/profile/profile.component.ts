import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlayersService } from '../../services/players.service';
import { IPlayer } from '../../interfaces/players.interface';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  player: IPlayer;

  constructor(
    private playerService: PlayersService,
    private activateRouter: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activateRouter.paramMap.subscribe(params => {
      const paramID = params.get('id');
      this.playerService
        .getPlayerByID(paramID)
        .subscribe((player: IPlayer) => (this.player = player));
    });
  }
}
