import { Component, OnInit } from '@angular/core';
import { IPlayer } from 'src/app/interfaces/players.interface';
import { PlayersService } from '../../services/players.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  playerList: IPlayer[];
  filter: string;


  constructor(private playerService: PlayersService) {
    this.playerList = [];
    this.filter = '';
  }

  ngOnInit() {
      this.playerService.getPlayers()
        .subscribe(playerList => this.playerList = playerList);
  }

}
