import { Component, OnInit } from '@angular/core';
import { IPlayer } from '../../interfaces/players.interface';
import { PlayersService } from '../../services/players.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {
  player: IPlayer;

  constructor(
    private playerService: PlayersService,
    private router: Router,
    private activateRouter: ActivatedRoute) { }

  ngOnInit() {
    this.activateRouter.paramMap.subscribe(params => {
      const paramID = params.get('id');
      this.playerService.getPlayerByID(paramID).subscribe((player: IPlayer) => this.player = player)
    });
  }

  onFormSubmit(player: IPlayer) {
    this.playerService.editPlayer(player).toPromise().then(() => {
      this.router.navigate(['/list']);
    });
  }
}
