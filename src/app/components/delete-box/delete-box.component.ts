import { Component, OnInit } from '@angular/core';
import { IPlayer } from '../../interfaces/players.interface';
import { PlayersService } from '../../services/players.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-delete-box',
  templateUrl: './delete-box.component.html',
  styleUrls: ['./delete-box.component.scss']
})
export class DeleteBoxComponent implements OnInit {
player: IPlayer;

  constructor(
    private playerService: PlayersService,
    private router: Router,
    private activateRouter: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activateRouter.paramMap.subscribe(params => {
      const id = params.get('id');
      this.playerService.getPlayerByID(id).subscribe((player: IPlayer) => this.player = player);
    });
  }

  onYes() {
    this.playerService.deletePlayer(this.player.id).subscribe();
  }

}






