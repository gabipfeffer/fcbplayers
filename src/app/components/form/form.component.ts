import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormBuilder} from '@angular/forms';
import { PlayersService } from '../../services/players.service';
import { IPlayer } from '../../interfaces/players.interface';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
playerForm: FormGroup;

@Input() id: string;
@Input() name: string;
@Input() status: string;
@Input() seasons: string;
@Input() jersey: number;
@Input() image: string;
@Input() playerurl: string;

@Output()formHandler = new EventEmitter<IPlayer>();

  constructor(
    private formBuilder: FormBuilder,
    private playerService: PlayersService
  ) {}

  ngOnInit() {
    this.playerForm = this.formBuilder.group({
       id: [this.id],
       name: [this.name],
       status: [this.status],
       seasons: [this.seasons],
       jersey: [this.jersey],
       image: [this.image],
       playerUrl: [this.playerurl],
    });
  }

  formSubmit(formValue: IPlayer) {
    this.formHandler.emit(formValue);
  }

}
