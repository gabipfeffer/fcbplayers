import { Injectable } from '@angular/core';
import { IPlayer } from '../interfaces/players.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PlayersService {
  playersUrl: string;
  playerProfileUrl: string;

  constructor(private http: HttpClient) {
    this.playersUrl = 'https://fcbplayers-api.herokuapp.com/';
  }

  getPlayers(): Observable<IPlayer[]> {
    return this.http.get<IPlayer[]>(this.playersUrl);
  }

  getPlayerByID(id: string): Observable<IPlayer> {
    const playerProfile = `${this.playersUrl}profile/${id}`;
    return this.http.get<IPlayer>(playerProfile);
  }

  addPlayer(player: IPlayer): Observable<IPlayer> {
    const postPlayerUrl = `${this.playersUrl}add`;
    return this.http.post<IPlayer>(postPlayerUrl, player, httpOptions);
  }

  editPlayer(player: IPlayer): Observable<IPlayer> {
    const editPlayerUrl = `${this.playersUrl}edit/${player.id}`;
    return this.http.put<IPlayer>(editPlayerUrl, player, httpOptions);
  }

  deletePlayer(id: string): Observable<{}> {
    const deletePlayerUrl = `${this.playersUrl}delete/${id}`;
    return this.http.delete(deletePlayerUrl, httpOptions);
  }
}
