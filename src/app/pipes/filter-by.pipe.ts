import { Pipe, PipeTransform } from '@angular/core';
import { IPlayer } from '../interfaces/players.interface';

@Pipe({
  name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {

  transform(list: IPlayer[], filter: string = ''): any {
    const lowerCaseFilter: string = filter.toLowerCase().trim();

    const filteredList: IPlayer[] = list.filter((el: IPlayer) => {
      if (el.name.toLowerCase().includes(lowerCaseFilter)) {
        return el.name.toLowerCase().includes(lowerCaseFilter)
      } else if (el.jersey.toString().toLowerCase().includes(lowerCaseFilter)) {
        return el.jersey.toString().toLowerCase().includes(lowerCaseFilter)
      }
    });

    return filteredList;
  }

}
