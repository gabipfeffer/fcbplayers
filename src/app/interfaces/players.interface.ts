export interface IPlayer {
    id: string;
    name: string;
    status: string;
    seasons: string;
    jersey: number;
    image: string;
    playerurl: string;
}